const badges = [
  {
    id: 1,
    badge:
      "https://img.shields.io/github/last-commit/rwietter/for-frontend?color=%230c9c5c&style=for-the-badge",
  },
  {
    id: 2,
    badge:
      "https://img.shields.io/github/license/rwietter/for-frontend?style=for-the-badge",
  },
  {
    id: 3,
    badge:
      "https://img.shields.io/github/stars/rwietter/for-frontend?style=for-the-badge",
  },
  {
    id: 4,
    badge:
      "https://img.shields.io/github/forks/rwietter/for-frontend?color=ff69b4&style=for-the-badge",
  },
]

export default badges
