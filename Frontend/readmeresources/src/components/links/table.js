const links = [
  {
    name: "Learning",
    id: "learning",
  },
  {
    name: "Challenges",
    id: "challenges",
  },
  {
    name: "Blogs",
    id: "blogs",
  },
  {
    name: "Web Development",
    id: "web",
  },
  {
    name: "Helpers",
    id: "helpers",
  },
  {
    name: "Deploy",
    id: "deploy",
  },
  {
    name: "Agile",
    id: "agile",
  },
  {
    name: "Project Management",
    id: "project",
  },
  {
    name: "Free Books",
    id: "books",
  },
  {
    name: "APIs",
    id: "api",
  },
  {
    name: "CSS",
    id: "css",
  },
  {
    name: "Typography",
    id: "api",
  },
  {
    name: "Design Inspiration",
    id: "design",
  },
  {
    name: "Testing",
    id: "testing",
  },
  {
    name: "Free Tools For Students",
    id: "students",
  },
]
export default links
