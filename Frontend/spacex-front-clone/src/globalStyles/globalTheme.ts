import { DefaultTheme } from 'styled-components';

const dark: DefaultTheme = {
  borderRadius: '5px',

  colors: {
    main: 'cyan',
    secondary: 'magenta'
  }
}

export { dark }
