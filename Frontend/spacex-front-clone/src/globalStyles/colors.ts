export default {
  primary: '#fff',
  secondary: 'rgba(2, 2, 2, 1)',
  third: 'rgb(160, 160, 160)',
  fourth: 'rgba(190, 190, 190, 1)',
};
