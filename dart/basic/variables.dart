void main() {
  var hello = "Hello,"; // infered string
  String world = "World"; // explict string
  print('$hello $world');
}