<span align="center">
  <h1>Algorithms and Data Structures</h1>
</span>

<br />
<br />
<br />

## Big O
- [Big-O Cheat Sheet](https://bigocheatsheet.io/?dark-mode=true)
- [Big-O Cheat Sheet - Know Thy Complexities!](https://www.bigocheatsheet.com/)
- [Algorithms & Data Structures](https://cooervo.github.io/Algorithms-DataStructures-BigONotation/index.html)

<br />
<br />

## Sorting
- [Algorithms & Data Structures](https://cooervo.github.io/Algorithms-DataStructures-BigONotation/algorithms.html)
- [Algorithm Visualizer](https://algorithm-visualizer.org/branch-and-bound/binary-search)
- [Sorting Animations](http://sorting.at/#)
- [Sorting Visualizer](https://karimelghamry.github.io/sorting-visualizer/)
- [Toptal sorting animations](https://www.toptal.com/developers/sorting-algorithms)
- [DSAnim](http://cathyatseneca.github.io/DSAnim/web/insertion.html)
- [Programiz](https://www.programiz.com/dsa/shell-sort)
- [Visualgo.net - sorting](https://visualgo.net/en/sorting)
- [Visualgo.net - hashTable](https://visualgo.net/en/hashtable)
- [USFCA - Sorting Algorithms](https://www.cs.usfca.edu/~galles/visualization/ComparisonSort.html)
- [Codesdope - bubleSort](https://www.codesdope.com/course/algorithms-bubble-sort/)
- [Codesdope - heapsort](https://www.codesdope.com/course/algorithms-heapsort/)
- [Codesdope - mergeSort](https://www.codesdope.com/course/algorithms-merge-sort/)
- [Codesdope - quickSort](https://www.codesdope.com/course/algorithms-quicksort/)
- [Big-O - wisc](http://pages.cs.wisc.edu/~mcw/cs367/lectures/bigO.html)
- [HashTable - wisc](http://pages.cs.wisc.edu/~mcw/cs367/lectures/hashtables.html)
- [HeapSort - wisc](http://pages.cs.wisc.edu/~mcw/cs367/lectures/heaps.html)
- [General Sorting - wisc](http://pages.cs.wisc.edu/~mcw/cs367/lectures/sorting.html)

<br />
<br />

## Free algorithms books 
- [Data Structures and Algorithms](https://cathyatseneca.gitbooks.io/data-structures-and-algorithms/content/)
- [Data Structures and Algorithm Analysis](http://people.cs.vt.edu/~shaffer/Book/JAVA3e20130328.pdf)
- [Algorithms, 4th Edition](https://algs4.cs.princeton.edu/20sorting/)

<br />
<br />

## Books
- [C Programming Language, Kernighan Brian W.; Ritchie Dennis](https://www.amazon.com.br/dp/B009ZUZ9FW/ref=dp-kindle-redirect?_encoding=UTF8&btkr=1)

<br />
<br />

## Articles & Study

- <strong> Teach Yourself Computer Science </strong>
  - [Teach Yourself Computer Science - CS education](https://teachyourselfcs.com/)

- <strong> A Guide to Parsing: Algorithms and Terminology </strong>
  - [A Guide to Parsing: Algorithms and Terminology](https://tomassetti.me/guide-parsing-algorithms-terminology/)

- <strong> Longest Common Subsequence </strong>
  - [Longest Common Subsequence - Git Diff](https://gabrielschade.github.io/2019/01/07/algoritmo-LCS.html)

- <strong> Big-O </strong>
  - [8 time complexities that every programmer should know](https://adrianmejia.com/most-popular-algorithms-time-complexity-every-programmer-should-know-free-online-tutorial-course/)

- <strong> Huffman Algorithm </strong>
  - [Huffman Coding](https://www.programiz.com/dsa/huffman-coding)
  - [Data Compression with Huffman’s Algorithm](https://freecontent.manning.com/data-compression-with-huffmans-algorithm/)

- <strong> Dijkstra's Algorithm </strong>
  - [Dijkstra's Algorithm](https://www.programiz.com/dsa/dijkstra-algorithm)
