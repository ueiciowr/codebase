# The Pratic Programmer

<p>Um guia com alguns dos <strong>melhores sites</strong>, para quem deseja <strong>aprender programação e desenvolvimento web</strong> a partir de <strong>leituras</strong> e <strong>muita prática</strong>.</p>

</br>

---

</br>

### Aprenda

- [Web Fundamentals](https://developers.google.com/web/fundamentals/)
- [Freecodecamp](https://www.freecodecamp.org/)
- [Hypertext Transfer Protocol (HTTP/1.1)](https://tools.ietf.org/html/rfc7231#section-9)
- [SQL Bolt](https://sqlbolt.com/)
- [CSS Tricks](https://css-tricks.com/centering-css-complete-guide/)
- [ECMAScript® 2016 Language Specification](https://262.ecma-international.org/7.0/#)
- [CSS Layout](https://csslayout.io/patterns/)
- [Rexegg](https://rexegg.com/regex-quickstart.html)
- [HTML Feference](https://htmlreference.io/)
- [CSS Feference](https://cssreference.io/)

</br>

---

</br>

### Fique atualizado

- [V8.dev](https://v8.dev/features)
- [Node Green](https://node.green/)

</br>

---

</br>

### Muita prática

- [Codewars](https://www.codewars.com/)
- [100 Days CSS](https://100dayscss.com/)
- [Frontend Mentor](https://www.frontendmentor.io/challenges)
- [Hacker Rank](https://www.hackerrank.com/)
- [Babeljs RPL](https://babeljs.io/en/repl)
- [Codier](https://codier.io/)
- [CSS Grid Generator](https://cssgrid-generator.netlify.app/)


